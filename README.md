# Authentication via JWT
`package gopkg.avesta.co.th/auth`

Package for JWT authentication middleware for Echo

*Index*
- [Variables](#variables)
- [type JWTInfo](#type-jwtinfo)
- [type DefaultUserClaims](#type-defaultuserclaims)
- [func Config(confg JWTInfo)](#func-config)
- [func GenerateToken](#func-generatetoken)
- [func JWTAuth](#func-jwtauth)

### Variables

```GO
var (
	jwtInfo   *JWTInfo
	jwtConfig middleware.JWTConfig
	parsedKey *rsa.PublicKey
	parsePriv *rsa.PrivateKey
)
```

### type JWTInfo

```go
type JWTInfo struct {
  // Private key path
  PublicKey  string
  // Public key path
  PrivateKey string
  // Custom Claims
  Claims     jwt.Claims
}
```

### type DefaultUserClaims

```GO
type DefaultUserClaims struct {
  ID   string         `json:"id"`
  Type int            `json:"type"`
  jwt.StandardClaims
}
```
### func Config

```GO
func Config(confg JWTInfo) {
}
```

### func GenerateToken

```GO
func GenerateToken(uc jwt.Claims) (string, error) 
}
```

### func JWTAuth

```GO
func JWTAuth() echo.MiddlewareFunc {
}
```

### Example
`server.go`
```GO
package main

import (
	"net/http"
	"time"

	"gopkg.avesta.co.th/auth"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func login(c echo.Context) error {
  username := c.FormValue("username")
  password := c.FormValue("password")

  if username == "jon" && password == "shhh!" {
  	// Create token
  	token := jwt.New(jwt.SigningMethodHS256)

  	// Set claims
    claims := auth.DefaultUserClaims{
      ID: 20,
      Type: 2,
      StandardClaims: jwt.StandardClaims{
  	  IssuedAt:  time.Now().Unix(),
  	  ExpiresAt: time.Now().Add(time.Minute * 30).Unix(),
  	},
    }

    // Generate encoded token and send it as response.
    t, err := auth.GenerateToken(claims)
    if err != nil {
    	return err
    }
    return c.JSON(http.StatusOK, map[string]string{
    	"token": t,
    })
  }

  return echo.ErrUnauthorized
}

func accessible(c echo.Context) error {
	return c.String(http.StatusOK, "Accessible")
}

func restricted(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	name := claims["name"].(string)
	return c.String(http.StatusOK, "Welcome "+name+"!")
}

func main() {
  e := echo.New()

  // Middleware
  e.Use(middleware.Logger())
  e.Use(middleware.Recover())

  // Login route
  e.POST("/login", login)

  // Unauthenticated route
  e.GET("/", accessible)
  // Set config
  authConfig := auth.JWTInfo{
    PublicKey: "path to public key",
    PrivateKey: "path to private key",
  }
  // Init Jwt with config
  auth.Config(authConfig)
  // Restricted group
  r := e.Group("/restricted")
  r.Use(auth.JWTAuth())
  r.GET("", restricted)

  e.Logger.Fatal(e.Start(":1323"))
}
```