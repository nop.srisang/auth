package authen

import (
	"crypto/rsa"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"io/ioutil"
	"log"
)

// JWTInfo type
type JWTInfo struct {
	PublicKey  string
	PrivateKey string
	Claims     jwt.Claims
}

// DefaultUserClaims Struct
type DefaultUserClaims struct {
	ID   string `json:"id"`
	Type int    `json:"type"`
	jwt.StandardClaims
}

// JWTAuth JTW verify middleware
func JWTAuth() echo.MiddlewareFunc {
	jwtConfig := setJwtConfig()
	middleWareFunc := middleware.JWTWithConfig(jwtConfig)
	return middleWareFunc
}

// Config return Authentication
func Config(confg JWTInfo) {
	jwtInfo = &JWTInfo{
		PrivateKey: confg.PrivateKey,
		PublicKey:  confg.PublicKey,
	}
	if confg.Claims == nil {
		jwtConfig.Claims = &DefaultUserClaims{}
	} else {
		jwtConfig.Claims = confg.Claims
	}

}

var (
	jwtInfo   *JWTInfo
	jwtConfig middleware.JWTConfig
	parsedKey *rsa.PublicKey
	parsePriv *rsa.PrivateKey
)

func setJwtConfig() middleware.JWTConfig {

	dat, err := ioutil.ReadFile(jwtInfo.PublicKey)
	if err != nil {
		log.Println(err)
	}
	parsedKey, err := jwt.ParseRSAPublicKeyFromPEM(dat)
	if err != nil {
		log.Println(err)
	}

	jwtConfig.SigningMethod = "RS512"
	jwtConfig.SigningKey = parsedKey

	return jwtConfig
}

// GenerateToken return token for user
func GenerateToken(uc jwt.Claims) (string, error) {
	if jwtInfo == nil {
		return "", errors.New("authentication need to be configured")
	}
	dat, err := ioutil.ReadFile(jwtInfo.PrivateKey)
	if err != nil {
		log.Println(err)
	}
	parsePriv, err = jwt.ParseRSAPrivateKeyFromPEM(dat)
	if err != nil {
		log.Println(err)
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS512, uc)

	return token.SignedString(parsePriv)
}
